<?php /** @noinspection PhpUnhandledExceptionInspection */


namespace App\MessageHandler;


use App\ImageOptimizer;
use App\Message\CommentMessage;
use App\Notification\CommentReviewNotification;
use App\Repository\CommentRepository;
use App\SpamChecker;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\NotificationEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Workflow\WorkflowInterface;

class CommentMessageHandler implements MessageHandlerInterface
{
    private $spamChecker;
    private $entityManager;
    private $commentRepository;
    private $bus;
    private $workflow;
    //private $mailer;
    private $notifier;
    private $optimizer;
    private $adminEmail;
    private $photoDir;
    private $logger;

    public function __construct(
        EntityManagerInterface $entityManager,
        SpamChecker $spamChecker,
        CommentRepository $commentRepository,
        MessageBusInterface $bus,
        WorkflowInterface $commentStateMachine,
        //MailerInterface $mailer,
        NotifierInterface $notifier,
        ImageOptimizer $optimizer,
        string $adminEmail,
        string $photoDir,
        LoggerInterface $logger = null
    )
    {
        $this->entityManager = $entityManager;
        $this->spamChecker = $spamChecker;
        $this->commentRepository = $commentRepository;
        $this->bus = $bus;
        $this->workflow = $commentStateMachine;
        //$this->mailer = $mailer;
        $this->notifier = $notifier;
        $this->optimizer = $optimizer;
        $this->adminEmail = $adminEmail;
        $this->photoDir = $photoDir;
        $this->logger = $logger;
    }

    public function __invoke(CommentMessage $message)
    {
        $comment = $this
            ->commentRepository
            ->find($message->getId())
        ;
        if (!$comment) {
            return;
        }

        /*
         * The new logic reads as follows:
         • If the accept transition is available for the comment in the message, check for spam;
        • Depending on the outcome, choose the right transition to apply;
        • Call apply() to update the Comment via a call to the setState() method;
        • Call flush() to commit the changes to the database;
        • Re-dispatch the message to allow the workflow to transition again.
         */
        if ($this->workflow->can($comment, 'accept'))
        {
            //$score = $this->spamChecker->getSpamScore($comment, $message->getContext());
            $score = 1;
            $transition = 'accept';

            if (2 === $score)
            {
                $transition = 'reject_spam';
            }
            elseif (1 === $score)
            {
                $transition = 'might_be_spam';
            }
            $this->workflow->apply($comment, $transition);
            $this->entityManager->flush();
            $this->bus->dispatch($message);
        }
        elseif (
            $this->workflow->can($comment, 'optimize')
        )
        {
            if($comment->getPhotoFilename())
            {
                $this->optimizer->resize($this->photoDir.'/'.$comment->getPhotoFilename());
                $this->workflow->apply($comment, 'optimize');

                $this->bus->dispatch($message);
            }
        }
        // ELSE Auto-validation for now. otherwise it must be done by admin.
        elseif (
            $this->workflow->can($comment, 'publish')
            || $this->workflow->can($comment, 'publish_ham')
        )
        {
            $this->workflow->apply($comment, 'publish');
            $this->bus->dispatch($message);

            /*$this->mailer->send((new NotificationEmail())
                ->subject('New comment posted')
                ->htmlTemplate('emails/comment_notification.html.twig')
                ->from($this->adminEmail)
                ->to($this->adminEmail)
                ->context(['comment' => $comment])
            );*/

            $this->notifier->send(
                new CommentReviewNotification($comment),
                ...$this->notifier->getAdminRecipients()
            );
        }
        elseif ($this->logger)
        {
            $this->logger->debug(
                'Dropping comment message',
                ['comment' => $comment->getId(), 'state' => $comment->getState()]
            );
        }

        $this->entityManager->flush();
    }
}