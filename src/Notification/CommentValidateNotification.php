<?php


namespace App\Notification;


use App\Entity\Comment;
use Symfony\Component\Notifier\Message\EmailMessage;
use Symfony\Component\Notifier\Notification\EmailNotificationInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\Recipient;

class CommentValidateNotification extends Notification implements EmailNotificationInterface
{
    private $comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
        parent::__construct('Comment validated.');
    }

    public function asEmailMessage(
        Recipient $recipient,
        string $transport = null
    ): ?EmailMessage
    {
        $message = EmailMessage::fromNotification($this, $recipient, $transport);
        $message->getMessage()
            ->htmlTemplate('emails/comment_validation.html.twig')
            ->context(['comment' => $this->comment])
        ;
        return $message;
    }

    public function getChannels(Recipient $recipient): array
    {
        if (preg_match('{\b(great|awesome)\b}i', $this->comment->getText())) {
            return ['email', /*'chat/slack'*/];
        }

        $this->importance(Notification::IMPORTANCE_LOW);
        return ['email'];
    }
}