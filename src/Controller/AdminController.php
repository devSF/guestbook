<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Message\CommentMessage;
use App\Notification\CommentValidateNotification;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\HttpCache\HttpCache;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Workflow\Registry;
use Twig\Environment;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    private $twig;
    private $entityManager;
    private $bus;
    private $notifier;

    public function __construct(
        Environment $twig,
        EntityManagerInterface $entityManager,
        MessageBusInterface $bus,
        NotifierInterface $notifier
    )
    {
        $this->twig = $twig;
        $this->entityManager = $entityManager;
        $this->bus = $bus;
        $this->notifier = $notifier;
    }

    /**
     * @Route("/{comment}/review", name="review_comment")
     * @param Comment $comment
     * @param Request $request
     * @param Registry $registry
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function reviewComment(
        Comment $comment,
        Request $request,
        Registry $registry
    )
    {
        $accepted = !$request->query->get('reject');
        $machine = $registry->get($comment);

        if ($machine->can($comment, 'publish'))
        {
            $transition = $accepted ? 'publish' : 'reject';
        }
        elseif ($machine->can($comment, 'publish_ham'))
        {
            $transition = $accepted ? 'publish_ham' : 'reject_ham';
        }
        else
        {
            return new Response('Comment already reviewed or not in the right state.');
        }
        $machine->apply($comment, $transition);
        $this->entityManager->flush();

        if ($accepted)
        {
            $this->notifier->send(
                new CommentValidateNotification($comment),
                ...$this->notifier->getAdminRecipients()
            );
            $this->bus->dispatch(new CommentMessage($comment->getId()));
        }

        return $this->render('admin/review.html.twig', [
            'transition' => $transition,
            'comment' => $comment,
        ]);
    }

    /**
     * @Route("/http-cache/{uri<.*>}", methods={"PURGE"})
     * @param KernelInterface $kernel
     * @param Request $request
     * @param string $uri
     * @return Response
     */
    public function purgeHttpCache(
        KernelInterface $kernel,
        Request $request,
        string $uri
    )
    {
        if ('prod' === $kernel->getEnvironment())
        {
            return new Response('KO', 400);
        }
        $store = (new class($kernel) extends HttpCache {})->getStore();
        $store->purge($request->getSchemeAndHttpHost().'/'.$uri);

        return new Response('Done');
    }
}
