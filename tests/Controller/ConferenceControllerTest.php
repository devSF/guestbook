<?php /** @noinspection PhpUndefinedClassInspection */

namespace App\Tests\Controller;

use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
//use Symfony\Component\Panther\PantherTestCase;

class ConferenceControllerTest extends WebTestCase
{
    public function testHomePage()
    {
        $client = static::createClient();
        // OR tests with a real browser (Google Chrome version 80)
         /*$client = static::createPantherClient([
             'external_base_uri' => $_SERVER['SYMFONY_DEFAULT_ROUTE_URL']
         ]);*/

        $crawler = $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains(
            'h2',
            'Give your feedback'
        );
    }

    public function testCommentSubmission()
    {
        $client = static::createClient();
        $client->request('POST', '/conference/amsterdam-2019');
        $client->submitForm('Submit', [
            'comment_form[author]' => 'Fabien',
            'comment_form[text]' => 'Some feedback from an automated functional test',
            'comment_form[email]' => $email = 'me@automat.ed',
            'comment_form[photo]' => dirname(__DIR__, 2).
                '/public/images/under-construction.gif',
        ]);

        $this->assertResponseRedirects();

        // simulate comment validation
        $comment = self::$container
            ->get(CommentRepository::class)
            ->findOneByEmail($email)
        ;
        $comment->setState('published');

        // From a PHPUnit test, you can get any service from the container via
        // self::$container->get() ; it also gives access to non-public services.
        self::$container
            ->get(EntityManagerInterface::class)
            ->flush()
        ;

        $crawler = $client->followRedirect();
        $this->assertSelectorExists(
            'div:contains("There are 2 comments.")'
        );

    }

    public function testConferencePage()
    {
        $client = static::createClient();

        // The request() method returns a Crawler instance that helps find
        //elements on the page (like links, forms, or anything you can reach
        //with CSS selectors or XPath)
        $crawler = $client->request('GET', '/');

        //Thanks to a CSS selector, we assert that we have conferences
        //listed on the homepage;
        $this->assertCount(2, $crawler->filter('h4'));

        $client->clickLink('View'); // OR
        //$client->click($crawler->filter('h4 + p a')->link());

        $this->assertPageTitleContains('Amsterdam');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains(
            'h2',
            'Amsterdam 2019'
        );

        //Finally, we assert that there is 5 comments on the page. div:contains()
        //is not a valid CSS selector, but Symfony has some nice additions,
        //borrowed from jQuery.
        $this->assertSelectorExists(
            'div',
            'There are 1 comments'
        );
    }


    /**
     * @dataProvider provideUrls
     * @param string $url
     */
    public function testPageIsSuccessful(string $url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertResponseIsSuccessful();
    }

    public function provideUrls()
    {
        return [
            ['/'],
            ['/blog'],
            ['/contact'],
            // ...
        ];
    }

    /*** if you’re testing a class in the src/Util/ directory,
         put the test in the tests/Util/ directory. ***/


    /***************** ALL ASSERTS ****************/

    //$this->assertEquals(42, $result);
    //$this->assertEquals(200, $client->getResponse()->getStatusCode());

    //$this->assertCount(2, $crawler->filter('h4'));

    //$this->assertPageTitleContains('Amsterdam');
    /***$this->assertResponseIsSuccessful();
     OR $this->assertTrue($client->getResponse()->isSuccessful());*/
    //$this->assertSelectorTextContains('h2', 'Amsterdam 2019');
    //$this->assertSelectorTextContains('html h1.title', 'Hello World');

    // asserts that the response matches a given CSS selector.
    //$this->assertGreaterThan(0, $crawler->filter('h1')->count());

    // test against the response content directly
    //$this->assertStringContainsString('Hello World', $client->getResponse()->getContent());

    // asserts that the "Content-Type" header is "application/json"
    //$this->assertResponseHeaderSame('Content-Type', 'application/json');

    // asserts that the response is a redirect to /demo/contact
    //$this->assertResponseRedirects('/demo/contact');

    // ...or check that the response is a redirect to any URL
    //$this->assertResponseRedirects();

    /*********** REQUEST METHOD ***********/

    // AJAX REQUEST
    // the required HTTP_X_REQUESTED_WITH header is added automatically
    //$client->xmlHttpRequest('POST', '/submit', ['name' => 'Fabien']);

    // submits a raw JSON string in the request body
    //$client->request(
    //'POST',
    //'/submit',
    //[],
    //[],
    //['CONTENT_TYPE' => 'application/json'],
    //'{"name":"Fabien"}'
    //);

    // Form submission with a file upload
    // use Symfony\Component\HttpFoundation\File\UploadedFile;
    //
    //$photo = new UploadedFile(
    //'/path/to/photo.jpg',
    //'photo.jpg',
    //'image/jpeg',
    //null
    //);
    //$client->request(
    //'POST',
    //'/submit',
    //['name' => 'Fabien'],
    //['photo' => $photo]
    //);

    // Perform a DELETE request and pass HTTP headers
    //$client->request(
    //'DELETE',
    //'/post/12',
    //[],
    //[],
    //['PHP_AUTH_USER' => 'username', 'PHP_AUTH_PW' => 'pa$$word']
    //);

    /*********** BROWSING *************/
    //The back() and forward() methods skip the redirects
    // that may have occurred when requesting a URL, as normal browsers do.

    //$client->back();
    //$client->forward();
    //$client->reload();

    // clears all cookies and the history
    //$client->restart();

    /*********** ACCESS INTERNAL OBJECTS **********/
    //$history = $client->getHistory();
    //$cookieJar = $client->getCookieJar();
    //$crawler = $client->getCrawler();

    /** the HttpKernel request instance **/
    //$request = $client->getRequest();
    /** the BrowserKit request instance **/
    //$request = $client->getInternalRequest();
    /** the HttpKernel response instance **/
    //$response = $client->getResponse();
    /** the BrowserKit response instance **/
    //$response = $client->getInternalResponse();


    /********** LOGGING IN USERS (AUTHENTICATION) **********/
    /** The loginUser() method was introduced in Symfony 5.1 **/

    public function testVisitingWhileLoggedIn()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('john.doe@example.com');

        // simulate $testUser being logged in
        // This method creates a special Symfony\Bundle\FrameworkBundle\Test\TestBrowserToken object
        // and stores in the session of the test client.
        $client->loginUser($testUser);

        // test e.g. the profile page
        $client->request('GET', '/profile');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Hello John!');
    }


    /*********** ACCESS PROFILER ************/
    /** IF you write functional tests that monitor your production servers **/

    //$client->enableProfiler();    // enables the profiler for the very next request
    //$crawler = $client->request('GET', '/lucky/number');
    //$profile = $client->getProfile();

    // check the number of requests
    //$this->assertLessThan(10, $profile->getCollector('db')->getQueryCount());

    // check the time spent in the framework
    //$this->assertLessThan(500, $profile->getCollector('time')->getDuration());

    /***********  ************/



}
